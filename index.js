const express = require("express");
const indexRouter = require('./routes/index.routes');
const movieRouter = require('./routes/movie.routes');
const cinemaRouter = require ('./routes/cinema.routes');

const { connectToDb } = require("./config/db");
connectToDb();

const PORT = 3000;
const server = express();

server.use(express.json());
server.use(express.urlencoded({ extended: false }));

server.use("/", indexRouter);
server.use('/movies', movieRouter);
server.use('/cinemas', cinemaRouter);

server.use('*', (req, res, next) => {
  const error = new Error('Route not found');
  error.status = 404;
  next(error); // Lanzamos la función next() con un error
  });
  
server.use((error, req, res, next)=> {
  const status = error.status || 500;
  const message = error.message || 'Unexpected error'

  return res.status(status).json(message);
});

const serverCallback = () => {
  console.log(`Server conected in http://localhost:${PORT}`);
};

server.listen(PORT, serverCallback);


