const express = require("express");
const Movie = require("../models/Movie");

const router = express.Router();

router.get("/", async (req, res, next) => {
  try {
    const movies = await Movie.find();
    return res.status(200).json(movies);
  } catch (error) {
    return next(error);
  }
});

router.get("/id/:id", async (req, res, next) => {
  try {
    const id = req.params.id;
    const movie = await Movie.findById(id);

    if (movie) {
      return res.status(200).json(movie);
    } else {
      return res.status(404).json("We don´t have this movie");
    }
  } catch (error) {
    return next(error);
  }
});
router.get("/title/:title", async (req, res, next) => {
  try {
    const title = req.params.title;
    const movieByTitle = await Movie.find({ 
      title: {$regex: new RegExp("^" + title.toLowerCase(), "i")}
     });
    // console.log('Pelicula ->', movie);
    if(!movieByTitle.length){
      return res.status(404).json('Movie not found')
    }else{
      return res.status(200).json(movieByTitle);
    }
    
  } catch (error) {
    return next(error);
  }
});
router.get("/genre/:genre", async (req, res, next) => {
  try {
    const genre = req.params.genre;
    const movieByGenre = await Movie.find({
      genre: {$regex: new RegExp("^" + genre.toLowerCase(), "i")}
     });

    if(!movieByGenre.length){
      return res.status(404).json('Genre not found')
    }else{
      return res.status(200).json(movieByGenre);
    }
  } catch (error) {
    return next(error);
  }
});
router.get("/year/:year", async (req, res, next) => {
  try {
    const year = req.params.year;
    const movieByYear = await Movie.find({ year: { $gt: year } });

    return res.status(200).json(movieByYear);
  } catch (error) {
    return next(error);
  }
});

router.post("/create", async (req, res, next) => {
  try {
    const newMovie = new Movie({
      title: req.body.title,
      director: req.body.director,
      year: req.body.year,
      genre: req.body.genre,
    });
    const createdMovie = await newMovie.save();
    return res.status(201).json(createdMovie);
  } catch (error) {
    return next(error);
  }
});

router.delete("/delete/:id", async (req, res, next) => {
  try {
    const { id } = req.params;
    const deletedMovie = await Movie.findByIdAndDelete(id);
    if (deletedMovie) {
      return res
        .status(200)
        .json(`You have deleted ${deletedMovie.title} succesfully`);
    } else {
      return res.status(404).json("This movie doesn't exist in our DB");
    }
  } catch (error) {
    return next(error);
  }
});

router.put("/edit/:id", async (req, res, next) => {
  try {
    const { id } = req.params;

    const modifiedMovie = new Movie(req.body);
    modifiedMovie._id = id;

    const updatedMovie = await Movie.findByIdAndUpdate(id, modifiedMovie, {
      new: true,
    });

    return res.status(200).json(updatedMovie);
  } catch (error) {
    return next(error);
  }
});

module.exports = router;
