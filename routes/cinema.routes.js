const express = require("express");
const Cinema = require("../models/Cinema");

const router = express.Router();

router.get("/", async (req, res, next) => {
  try {
    const cinemas = await Cinema.find().populate("movies");
    return res.status(200).json(cinemas);
  } catch (error) {
    return next(error);
  }
});

router.post("/create", async (req, res, next) => {
  try {
    const newCinema = await new Cinema({
      name: req.body.name,
      location: req.body.location,
      movies: [],
    });
    const createdCinema = await newCinema.save();
    return res.status(201).json(createdCinema);
  } catch (error) {
    return next(error);
  }
});

router.put("/add-movie", async (req, res, next) => {
  try {
    const { cinemaId, movieId } = req.body;

    const updatedCinema = await Cinema.findByIdAndUpdate(
      cinemaId,
      { $addToSet: { movies: movieId}},
      { new: true }
    );
    return res.status(200).json(updatedCinema);
  } catch (error) {
    return next(error);
  }
});

router.delete("/delete/:id", async(req, res, next)=>{
    try{
        const { id } = req.params;
        const deletedCinema = await Cinema.findByIdAndDelete(id);
        if (deletedCinema) {
            return res
              .status(200)
              .json(`You have deleted ${deletedCinema.name} succesfully`);
          } else {
            return res.status(404).json("This cinema not exist in our DB");
          }

    }catch(error){
        return next(error);
    }
})

module.exports = router;
