const mongoose = require("mongoose");

const DB_URL = "mongodb://localhost:27017/proyecto-1-node";
const CONFIG_DB = { useNewUrlParser: true, useUnifiedTopology: true };

const connectToDb = async () => {
  try {
    const response = await mongoose.connect(DB_URL, CONFIG_DB);
    const { host, port, name } = response.connection;
    console.log(`Conected to ${name} in ${host}:${port}`);
  } catch (error) {
    console.log("Error conecting to db", error);
  }
};

// connectToDb();
module.exports = {
  DB_URL,
  CONFIG_DB,
  connectToDb,
};

// mongoose
//   .connect(DB_URL, {
//     useNewUrlParser: true,
//     useUnifiedTopology: true,
//   })
//   .then((response) => {
//     const { host, port, name } = response.connection;
//     console.log(`Conectado a ${host} en ${port}:${name}`);
//   })
//   .catch((error) => console.log("Error conectando a la BBDD", error));

// const DB_URL = "mongodb://localhost:27017/proyecto-1-node";

// const connect = async () => {
//   try {
//     await mongoose.connect(DB_URL, CONFIG_DB);
//     const { host, port, name } = response.connection;
//     console.log(`Conectado a ${name} en ${host}:${port}`);
//   } catch (error) {
//     console.log("Error conectando a la BBDD", error);
//   }
// };

